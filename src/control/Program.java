package control;

import java.util.Random;
import java.util.Scanner;

import model.IntegrantNode;
import model.ListVal;
import model.SimpleLinkedList;

public class Program {

	public static void main(String[] args) {
		// catching the size of list
		Scanner input = new Scanner(System.in);
		
    	System.out.println("informe a quantidade de participantes: ");
		int total = input.nextInt();
		
		ListVal listVal = new ListVal(total);
		
		// filling the list
		for (int i = 1; i <= total; i++) {
			System.out.print("Informe o NOME do "+i+"º participante: ");
			String name = input.next();
			
			listVal.addOnList(name);
		}
				
		System.out.println("\nParticipantes do jogo: ");
		System.out.println(listVal.showList('d'));
		
		System.out.println("\n---------------INÍCIO DO JOGO---------------");
		
		Random random = new Random();
		int totalAux = total-1;
		while (listVal.getFna().getNext() != null) {
			for (int i = 1; i <= totalAux; i++) {
				int rand = random.nextInt(1000);
				
				while (rand == 0)
					rand = random.nextInt(1000);
				
				System.out.println("\n"+i+"ª RODADA");
				
				int realPosition = rand%total;
				if (realPosition == 0)
					realPosition = total;
				System.out.println("\nNúmero sorteado-> "+rand+" | posição a ser removida ["+realPosition+"]");
				
				System.out.println("Lista de PARTICPANTES antes da eliminação:");
				System.out.println(listVal.showList('d'));
				
				// deleting choose integrant
				IntegrantNode fnaAux = listVal.getFna();
				int reversePostition = (total+1)-(realPosition); // for count real position to remove on real position
				for (int j = 1; j < reversePostition; j++)
					fnaAux = fnaAux.getNext();
				total--;
				listVal.removeFromList(fnaAux.getName());
				System.out.println("Participante ELIMINADO-> "+fnaAux.getName());
				
				System.out.println("Lista de PARTICPANTES depois da eliminação:");
				System.out.println(listVal.showList('d'));
			}
		}	
		
		System.out.println("\n---------------FIM DO JOGO---------------");
		System.out.println("Participante vencedor -> "+listVal.getFna().getName());		
	}

}
